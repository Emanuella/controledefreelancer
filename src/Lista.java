
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

 

public class Lista {
    public void listar(JList listaFreelancer){
        try {
            listaFreelancer.removeAll();
            DefaultListModel dfm= new DefaultListModel ();
            Connection c= conexao.obterConexao();
            String SQL="Select*fromSc.controledefreelancer";
            PreparedStatement ps=c.prepareStatement(SQL);
            ResultSet rs= ps.executeQuery(SQL);
            while (rs.next()){
                dfm.addElement(rs.getString("Nome"));
            }
            listaFreelancer.setModel(dfm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Lista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
