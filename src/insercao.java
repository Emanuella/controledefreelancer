
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class insercao {
    public void inserir (String nome,String idade, String cpf,String certificado){
        try {
            Connection C=conexao.obterConexao();
            PreparedStatement ps=C.prepareStatement("INSERT INTO SC_CONTROLEDEFREELANCER.FREELANCER(nome,cpf,idade, certificado)"  + "VALUES(?,?,?,?);");
            ps.setString(1,nome);
            ps.setInt(3,Integer.valueOf(idade));
            ps.setInt(2,Integer.valueOf(cpf));
            ps.setInt(4,Integer.valueOf(certificado));
            ps.executeUpdate();
           C.close();

        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void inserir2 (String nome,String cpf, String cnpj,String idade){
        try {
            Connection C=conexao.obterConexao();
            PreparedStatement ps=C.prepareStatement("INSERT INTO SC_CONTROLEDEFREELANCER.patrao(nome,cpf,cnpj,idade)"  + "VALUES(?,?,?,?);");
            ps.setString(1,nome);
            ps.setInt(2,Integer.valueOf(cpf));
            ps.setInt(3,Integer.valueOf(cnpj));
            ps.setInt(4,Integer.valueOf(idade));
            ps.executeUpdate();
           C.close();

        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
     public void adicionarFornecedor (String nome,String cnpj, String contato,String endereco, String contrato){
        try {
            Connection C=conexao.obterConexao();
            PreparedStatement ps=C.prepareStatement("INSERT INTO SC_CONTROLEDEFREELANCER.fornecedordouniforme(nome,contato,cnpj,contrato,endereco)"  + "VALUES(?,?,?,?,?);");
            ps.setString(1,nome);
            ps.setInt(3,Integer.valueOf(cnpj));
            ps.setInt(2,Integer.valueOf(contato));
            ps.setInt(4,Integer.valueOf(contrato));
            ps.setInt(5,Integer.valueOf(endereco));
            ps.executeUpdate();
           C.close();

        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    
     }

  
 }
     